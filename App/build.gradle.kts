plugins {
    id("bootstrapp.android.application")
    kotlin("kapt")
}

android {
    defaultConfig {
        applicationId = "fr.jpbarbaud.bootstrapp"
        versionCode = 1
        versionName = "0.0.1"
    }
}